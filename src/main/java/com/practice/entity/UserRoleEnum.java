package com.practice.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public enum UserRoleEnum {
    ADMIN(1, "ADMIN"),
    USER(2, "USER");

    private int id;
    private String role;

}
