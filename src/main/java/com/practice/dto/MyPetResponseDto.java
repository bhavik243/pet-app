package com.practice.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MyPetResponseDto {

    private Long id;
    private String name;
    private String place;
    private Integer age;
    private Long boughtDate;
}
