package com.practice.repository;

import com.practice.entity.Pet;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface PetRepository extends JpaRepository<Pet,Long>{

	@Query(value = "SELECT * FROM pet p WHERE p.name=:name or p.place=:place or p.age=:age", nativeQuery = true)
	Page<Pet> findPetBySearch(@Param("name") String name,
							   @Param("place") String place,
							   @Param("age") Integer age,
							   Pageable pageable);

	Page<Pet> findByOwnerId(@Param("ownerId") Long ownerId, Pageable pageable);

}
